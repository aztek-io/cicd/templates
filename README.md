# ci-tempaltes

## About

This is meant to be a cookie cutter style of pipeline shared libraries.

## How to use

In project where you'd like to invoke a pipeline, update the `.gitlab-ci.yml` file like so:

```
include:
- project: 'aztek-io/cicd/templates'
  ref: v0.10.0
  file: '/ci/docker.yaml'
```

Project icon from [ICONS8](https://icons8.com/license)
